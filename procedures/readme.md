# Major Network Changes Procedures

## Definition
Any network change that removes network access from a critical device

## Scheduling
Major network must be scheduled a week in advance after a plan has been laid out for making the change

## Step 1

1. Identify all critical infastructure and document them
2. Identify what devices will be impacted by the change and document which ones are critical and which ones are not critical
3. Document current settings of any devices where the changes will occur including taking a backup config of each device
4. Document what changes will be made to each device
5. Layout a plan of what order the changes will happen
6. Notify the client of when the changes are scheduled to happen

## Step 2

1. Let client know that we are onsite
2. Implement the changes following the plan developed in Step 1
3. Ensure that all critical devices impacted by the network change are back online
4. If time allows check on all non critical devices otherwise let client know that we check back later to make sure that everything working
5. Apply network changes to the current network documentation

[Back](../readme.md)